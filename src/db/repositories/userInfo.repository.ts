import { EntityRepository, InsertResult, Repository } from 'typeorm';
import { UserInfo } from '../entities/userInfo.entity';

@EntityRepository(UserInfo)
export class UserInfoRepository extends Repository<UserInfo> {
  public async getUserInfoByName(name: string): Promise<UserInfo | undefined> {
    return this.findOne({ where: { name } });
  }

  public async createUserInfo(name: string): Promise<InsertResult | undefined> {
    return this.createQueryBuilder()
      .insert()
      .into('userInfo', ['name'])
      .values({ name })
      .execute();
  }

  public async getAllUserInfos(): Promise<UserInfo[] | undefined> {
    return this.find();
  }
}
