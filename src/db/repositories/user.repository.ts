import {
  EntityRepository,
  FindManyOptions,
  InsertResult,
  Repository
} from 'typeorm';
import { User } from '../entities/user.entity';
import { BaseRepository } from './base/baseRepository';

@EntityRepository(User)
export class UserRepository extends BaseRepository<User> {
  constructor() {
    super();
  }
  public async getUserByEmail(email: string): Promise<User | undefined> {
    return this.findOne({ where: { email } });
  }

  public async createUser(data: any): Promise<InsertResult | undefined> {
    return this.createQueryBuilder()
      .insert()
      .into(User)
      .values(data)
      .execute();
  }
}
