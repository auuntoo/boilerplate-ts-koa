import { ObjectLiteral, Repository } from 'typeorm';
import { paginate, paginationOptions } from '../../../utils';

export class BaseRepository<Entity extends ObjectLiteral> extends Repository<
  Entity
> {
  constructor() {
    super();
  }

  public async getAll(options: any): Promise<any | undefined> {
    const pOptions: any = paginationOptions(options);
    pOptions.where = { ...options };

    const payload = await this.findAndCount(pOptions);
    return paginate(pOptions, payload);
  }
}
