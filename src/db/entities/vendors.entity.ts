import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Vendors {
  @PrimaryGeneratedColumn('uuid') public uuid?: string;

  @Column({ nullable: false, unique: true })
  public name: string;

  @Column({ nullable: false, unique: true })
  public email: string;

  constructor(name: string, email: string) {
    this.name = name;
    this.email = email;
  }
}
