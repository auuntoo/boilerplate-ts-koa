import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class UserInfo {
  @PrimaryGeneratedColumn('uuid') public uuid?: string;

  @Column({ nullable: false, unique: true })
  public name: string;

  constructor(name: string) {
    this.name = name;
  }
}
