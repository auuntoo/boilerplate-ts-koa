import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid') public uuid?: string;

  @Column({ nullable: false, unique: true })
  public username: string;

  @Column({ nullable: false, unique: true })
  public email: string;

  @Column({ nullable: false, unique: false })
  public password: string;

  constructor(username: string, email: string, password: string) {
    this.username = username;
    this.email = email;
    this.password = password;
  }
}
