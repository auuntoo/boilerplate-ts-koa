import { Context } from 'koa';
import {
  Body,
  Ctx,
  Get,
  JsonController,
  Post,
  QueryParams,
  Req
} from 'routing-controllers';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { User } from '../../db/entities/';
import { UserRepository } from '../../db/repositories';

@JsonController('/users')
export class UserController {
  constructor(@InjectRepository() private userRepository: UserRepository) {}

  @Get('/')
  public async getUsers(@QueryParams() options: any) {
    return this.userRepository.getAll(options);
  }

  @Post('/')
  public async createUser(@Ctx() ctx: Context, @Body() data: User) {
    // const { username, email, password } = data;

    // const newUser = new User(username, email, password);

    // newUser.username = username;
    // newUser.email = email;
    // newUser.password = password;

    try {
      const createdUser: User = await this.userRepository.save(data);
      return createdUser;
    } catch (err) {
      console.log(err);
    }
  }
}
