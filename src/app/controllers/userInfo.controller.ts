import { Context } from 'koa';
import { Body, Ctx, Get, JsonController, Post } from 'routing-controllers';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { UserInfo } from '../../db/entities/';
import { UserInfoRepository } from '../../db/repositories';

@JsonController('/userInfos')
export class UserInfoController {
  constructor(
    @InjectRepository() private userInfoRepository: UserInfoRepository
  ) {}

  @Get('/')
  public async getUserInfos() {
    return this.userInfoRepository.getAllUserInfos();
  }

  @Post('/')
  public async createUserInfo(@Ctx() ctx: Context, @Body() data: UserInfo) {
    const { name } = data;

    const newUserInfo = new UserInfo(name);

    newUserInfo.name = name;

    try {
      const createdUserInfo: UserInfo = await this.userInfoRepository.save(
        newUserInfo
      );
      return createdUserInfo;
    } catch (err) {
      console.log(err);
    }
  }
}
