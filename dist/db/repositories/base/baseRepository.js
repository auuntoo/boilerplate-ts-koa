"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const utils_1 = require("../../../utils");
class BaseRepository extends typeorm_1.Repository {
    constructor() {
        super();
    }
    getAll(options) {
        return __awaiter(this, void 0, void 0, function* () {
            const pOptions = utils_1.paginationOptions(options);
            pOptions.where = Object.assign({}, options);
            const payload = yield this.findAndCount(pOptions);
            return utils_1.paginate(pOptions, payload);
        });
    }
}
exports.BaseRepository = BaseRepository;
//# sourceMappingURL=../../../../src/dist/db/repositories/base/baseRepository.js.map